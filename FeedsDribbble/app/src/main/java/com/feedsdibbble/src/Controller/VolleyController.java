package com.feedsdibbble.src.Controller;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.feedsdibbble.src.View.Callback.VolleyCallback;

import org.json.JSONObject;

/**
 * Created by wildsonsantos on 17/06/15.
 */
public class VolleyController {
    private JsonObjectRequest jsObjectRequest;
    private RequestQueue requestQueue;
    public void setRequestVolley(Context context, final VolleyCallback volleyCallBack, String url){
        requestQueue = Volley.newRequestQueue(context);
        jsObjectRequest = new JsonObjectRequest(Request.Method.GET, url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                volleyCallBack.retornoVolleyCallback(jsonObject);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                volleyCallBack.erroVolleyCallback(volleyError.getMessage());

            }
        });

        requestQueue.add(jsObjectRequest);

    }
}
