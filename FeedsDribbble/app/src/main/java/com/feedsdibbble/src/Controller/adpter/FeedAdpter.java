package com.feedsdibbble.src.Controller.adpter;

import android.content.Context;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.feedsdibbble.src.Controller.network.VolleyHelper;
import com.feedsdibbble.src.Model.Item;
import com.feedsdibbble.src.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wildsonsantos on 17/06/15.
 */
public class FeedAdpter extends BaseAdapter {
    private List<Item> listFeed = new ArrayList<Item>();
    private Context context;

    public FeedAdpter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return listFeed.size();
    }

    @Override
    public Object getItem(int position) {
        return listFeed.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Item  item = (Item)getItem(position);

        View view = LinearLayout.inflate(context, R.layout.item, null);

        TextView name = (TextView) view.findViewById(R.id.textViewItemName);
        TextView desc = (TextView) view.findViewById(R.id.textViewItem);
        NetworkImageView iv_thumb = (NetworkImageView) view.findViewById(R.id.iv_thumb);

        Spanned titulo = null;
        Spanned descricao = null;


        if(!item.getTitle().equalsIgnoreCase("")){
            titulo = Html.fromHtml(item.getTitle().toString());
            name.setText(titulo);
        }else{
            name.setVisibility(View.GONE);
        }

        if(!item.getDescription().equalsIgnoreCase("")){
            descricao = Html.fromHtml(item.getDescription());
            desc.setText(descricao);
        }else{
            desc.setVisibility(View.GONE);
        }


        Log.e("<<<URL>>>", item.getImage_url());

        if(!item.getImage_teaser_url().equalsIgnoreCase("")){
            iv_thumb.setImageUrl(item.getImage_teaser_url(), VolleyHelper.getImageLoader());
        }else{
            iv_thumb.setVisibility(View.GONE);
        }

        return view;
    }

    public void addItens(List<Item> list){
        listFeed.clear();
        listFeed = list;
        notifyDataSetChanged();
    }
}
