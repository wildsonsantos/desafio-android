package com.feedsdibbble.src.Controller.network;

import android.content.Context;

import com.android.volley.toolbox.ImageLoader;
import com.feedsdibbble.src.Controller.network.image.BitmapLruCache;
import com.feedsdibbble.src.util.AppDelegation;

/**
 * Helper class that is used to provide references to initialized
 * RequestQueue(s) and ImageLoader(s)
 */
public class VolleyHelper {
	
	private static final int MAX_IMAGE_CACHE_ENTIRES = 150;
	private static ImageLoader mImageLoader;

	private VolleyHelper() {
	}

	public static void init(Context context) {
		mImageLoader = new ImageLoader(AppDelegation.getAppDelegation().getRequestQueue(context), new BitmapLruCache(
				MAX_IMAGE_CACHE_ENTIRES));
	}

	/**
	 * Returns instance of ImageLoader initialized with {@see FakeImageCache}
	 * which effectively means that no memory caching is used. This is useful
	 * for images that you know that will be show only once.
	 */
	public static ImageLoader getImageLoader() {
		if (mImageLoader != null) {
			return mImageLoader;
		} else {
			throw new IllegalStateException("ImageLoader not initialized");
		}
	}
	
}
