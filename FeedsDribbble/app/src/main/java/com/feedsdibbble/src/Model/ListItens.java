package com.feedsdibbble.src.Model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wildsonsantos on 17/06/15.
 */
public class ListItens {
    public int pages;
    public List<Item> shots = new ArrayList<Item>();

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public List<Item> getShots() {
        return shots;
    }

    public void setShots(List<Item> shots) {
        this.shots = shots;
    }
}
