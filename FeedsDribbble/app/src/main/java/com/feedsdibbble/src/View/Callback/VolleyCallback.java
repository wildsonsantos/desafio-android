package com.feedsdibbble.src.View.Callback;

import org.json.JSONObject;

/**
 * Created by wildsonsantos on 17/06/15.
 */
public interface VolleyCallback {
    void retornoVolleyCallback(JSONObject jsonObject);
    void erroVolleyCallback(String msg);
}
