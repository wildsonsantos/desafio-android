package com.feedsdibbble.src.View;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.feedsdibbble.src.Controller.network.VolleyHelper;
import com.feedsdibbble.src.Model.Item;
import com.feedsdibbble.src.R;
import com.feedsdibbble.src.util.AppDelegation;

public class DetalhesActivity extends ActionBarActivity {
    private TextView name, desc;
    private NetworkImageView iv_thumb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhes);

        name = (TextView) findViewById(R.id.textViewItemName);
        desc = (TextView) findViewById(R.id.textViewItem);
        iv_thumb = (NetworkImageView) findViewById(R.id.iv_thumb);


        Spanned titulo = null;
        Spanned descricao = null;

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Item item = AppDelegation.getAppDelegation().getItemSelecionado();


        if(!item.getTitle().equalsIgnoreCase("")){
            titulo = Html.fromHtml(item.getTitle().toString());
            name.setText(titulo);
        }else{
            name.setVisibility(View.GONE);
        }

        if(!item.getDescription().equalsIgnoreCase("")){
            descricao = Html.fromHtml(item.getDescription());
            desc.setText(descricao);
        }else{
            desc.setVisibility(View.GONE);
        }


        Log.e("<<<URL>>>", item.getImage_url());

        if(!item.getImage_teaser_url().equalsIgnoreCase("")){
            iv_thumb.setImageUrl(item.getImage_400_url(), VolleyHelper.getImageLoader());
        }else{
            iv_thumb.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.menu_detalhes, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return false;
        }

        return super.onOptionsItemSelected(item);
    }
}
