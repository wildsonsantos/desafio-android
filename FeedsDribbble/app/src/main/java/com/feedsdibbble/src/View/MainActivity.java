package com.feedsdibbble.src.View;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.feedsdibbble.src.Controller.adpter.FeedAdpter;
import com.feedsdibbble.src.Controller.network.VolleyHelper;
import com.feedsdibbble.src.Model.ListItens;
import com.feedsdibbble.src.R;
import com.feedsdibbble.src.View.Callback.VolleyCallback;
import com.feedsdibbble.src.util.AppDelegation;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;


public class MainActivity extends ActionBarActivity implements VolleyCallback, AdapterView.OnItemClickListener{
    private String url = "http://api.dribbble.com/shots/popular?page=";
    private int page = 0;

    private ProgressDialog mDialog;
    private Gson gson = new Gson();
    private ListItens listItens = null;
    private ListView listviewFeed;
    private FeedAdpter feedAdpter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        VolleyHelper.init(this);

        listviewFeed = (ListView) findViewById(R.id.listviewFeed);
        feedAdpter = new FeedAdpter(getBaseContext());

        listviewFeed.setAdapter(feedAdpter);
        listviewFeed.setOnItemClickListener(this);

        page++;
        carregaFeed();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    private void carregaFeed(){

        mDialog = ProgressDialog.show(this, "", "Aguarde...", true);

        Log.e("<<<URL>>>", url+page);

        AppDelegation.getAppDelegation().getVolleyController().setRequestVolley(getBaseContext(), MainActivity.this, url+page);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        switch (id){
            case R.id.action_left:
                if (page > 1){
                    page--;
                    carregaFeed();
                }
                break;
            case R.id.action_right:
                if (listItens.getPages() > 1){
                    page++;
                    carregaFeed();
                }
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void retornoVolleyCallback(JSONObject jsonObject) {
        listItens = new ListItens();
        listItens = gson.fromJson(jsonObject.toString(), ListItens.class);
        feedAdpter.addItens(listItens.getShots());
        listviewFeed.setAdapter(feedAdpter);
        mDialog.dismiss();
    }

    @Override
    public void erroVolleyCallback(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            AppDelegation.getAppDelegation().setItemSelecionado(listItens.getShots().get(position));
            startActivity(new Intent(this, DetalhesActivity.class));
    }
}
