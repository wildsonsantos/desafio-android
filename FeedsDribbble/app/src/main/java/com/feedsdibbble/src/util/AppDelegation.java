package com.feedsdibbble.src.util;

import android.app.Application;
import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.feedsdibbble.src.Controller.VolleyController;
import com.feedsdibbble.src.Model.Item;

import org.json.JSONObject;

/**
 * Created by wildsonsantos on 17/06/15.
 */
public class AppDelegation extends Application{
    public static AppDelegation appDelegation;
    public VolleyController volleyController;
    private RequestQueue mRequestQueue;
    public JSONObject jsonObjectLogin = null;
    public Item itemSelecionado = null;


    @Override
    public void onCreate() {

        volleyController = new VolleyController();

        super.onCreate();
    }

    public static AppDelegation getAppDelegation() {
        if(appDelegation == null){
            appDelegation = new AppDelegation();
        }
        return appDelegation;
    }

    public static void setAppDelegation(AppDelegation appDelegation) {
        AppDelegation.appDelegation = appDelegation;
    }

    public VolleyController getVolleyController() {

        volleyController = new VolleyController();

        return volleyController;
    }

    public void setVolleyController(VolleyController volleyController) {
        this.volleyController = volleyController;
    }

    public RequestQueue getRequestQueue(Context context) {
        // lazy initialize the request queue, the queue instance will be
        // created when it is accessed for the first time
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(context);
        }

        return mRequestQueue;
    }

    public Item getItemSelecionado() {
        return itemSelecionado;
    }

    public void setItemSelecionado(Item itemSelecionado) {
        this.itemSelecionado = itemSelecionado;
    }
}
